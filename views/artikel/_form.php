<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Artikel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="artikel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'isi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'foto')->fileInput() ?>

    <?= $form->field($model, 'jenis')->dropDownList([
            'Berita' => 'Berita', 'Hiburan' => 'Hiburan', 'Opini' => 'Opini'
    ]) ?>

    <?= $form->field($model, 'lolos_edit')->radioList([
            'Lolos' => 'Lolos', 'Tidak Lolos' => 'Tidak Lolos'
    ]) ?>

    <?= $form->field($model, 'id_penulis')->dropDownList(\app\models\Artikel::getDropdownData()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
