<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Penulis]].
 *
 * @see Penulis
 */
class PenulisQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Penulis[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Penulis|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
