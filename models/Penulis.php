<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "penulis".
 *
 * @property string $nama
 * @property string $asal
 *
 * @property Artikel[] $artikels
 */
class Penulis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'penulis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'asal'], 'required'],
            [['nama', 'asal'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nama' => 'Nama Penulis',
            'asal' => 'Asal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArtikels()
    {
        return $this->hasMany(Artikel::className(), ['id_penulis' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return PenulisQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PenulisQuery(get_called_class());
    }
}
