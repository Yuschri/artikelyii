<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Artikel]].
 *
 * @see Artikel
 */
class ArtikelQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Artikel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Artikel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
