<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "artikel".
 *
 * @property int $id
 * @property string $judul
 * @property string $isi
 * @property string $foto
 * @property string $jenis
 * @property int $lolos_edit
 * @property int $id_penulis
 * @property int $counter
 *
 * @property Penulis $penulis
 */
class Artikel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'artikel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'isi', 'jenis', 'lolos_edit', 'id_penulis'], 'required'],
            [['isi'], 'string'],
            [['id_penulis', 'counter'], 'integer'],
            [['judul', 'jenis'], 'string', 'max' => 100],
            [['id_penulis'], 'exist', 'skipOnError' => true, 'targetClass' => Penulis::className(), 'targetAttribute' => ['id_penulis' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'foto' => 'Foto',
            'jenis' => 'Jenis',
            'lolos_edit' => 'Lolos Edit',
            'id_penulis' => 'Penulis',
            'counter' => 'Counter',
        ];
    }

    public static function getDropdownData()
    {
        $penulis = Penulis::find()->all();

        foreach ($penulis as $p){
            $data[$p->id] = $p->nama;
        }

        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPenulis()
    {
        return $this->hasOne(Penulis::className(), ['id' => 'id_penulis']);
    }

    /**
     * {@inheritdoc}
     * @return ArtikelQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ArtikelQuery(get_called_class());
    }
}
